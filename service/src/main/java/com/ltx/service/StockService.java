package com.ltx.service;

import com.ltx.service.configuration.APIConfiguration;
import com.ltx.service.pojo.StockDTO;
import com.ltx.service.resposity.RepositoryInterface;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class StockService implements APIConfiguration {
    private RepositoryInterface service;

    public StockService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(RepositoryInterface.class);

    }

    /**
     * 获取所有的股票数据
     *
     * @param appId
     * @param sign
     * @param stocks
     * @param needIndex
     */
    public StockDTO getStockInfo(String appId, String sign, String stocks, String needIndex) {
        Map<String, String> paramMap = new ConcurrentHashMap();
        paramMap.put("showapi_appid", appId);
        paramMap.put("showapi_sign", sign);
        paramMap.put("stocks", stocks);
        paramMap.put("needIndex", needIndex);
        Call<StockDTO> repositoryCall = service.getRealTimeStockData(paramMap);
        Response<StockDTO> response = null;
        try {
            response = repositoryCall.execute();
        } catch (IOException e) {
            System.out.println(e);
        }
        return response.body();
    }
}
