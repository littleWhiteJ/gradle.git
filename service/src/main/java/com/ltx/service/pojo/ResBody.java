package com.ltx.service.pojo;

import java.util.List;

public class ResBody {
    private int ret_code;
    private List<Lists> list;
    public void setRet_code(int ret_code) {
        this.ret_code = ret_code;
    }
    public int getRet_code() {
        return ret_code;
    }

    public void setLists(List<Lists> lists) {
        this.list = lists;
    }
    public List<Lists> getLists() {
        return list;
    }
}
