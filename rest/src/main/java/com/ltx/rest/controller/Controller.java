package com.ltx.rest.controller;

import com.ltx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usr")
public class Controller {
    @Autowired
    private UserService userService;

    @PostMapping("/basic/131-46")
    public Object getUserInfo() {
        System.out.println("jinglaiguo");
        return userService.getUserInfo();
    }
}
